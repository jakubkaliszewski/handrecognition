\babel@toc {polish}{}
\contentsline {chapter}{\numberline {1}Wst\IeC {\k e}p}{5}{chapter.1}% 
\contentsline {section}{\numberline {1.1}Zawarto\IeC {\'s}\IeC {\'c} pracy}{6}{section.1.1}% 
\contentsline {chapter}{\numberline {2}Czym jest g\IeC {\l }\IeC {\k e}bokie uczenie?}{8}{chapter.2}% 
\contentsline {section}{\numberline {2.1}Sieci konwolucyjne}{10}{section.2.1}% 
\contentsline {chapter}{\numberline {3}U\IeC {\.z}yte technologie i struktura projektu}{12}{chapter.3}% 
\contentsline {section}{\numberline {3.1}U\IeC {\.z}yte technologie}{12}{section.3.1}% 
\contentsline {section}{\numberline {3.2}Struktura projektu}{13}{section.3.2}% 
\contentsline {section}{\numberline {3.3}Interfejs graficzny programu HandyNET}{15}{section.3.3}% 
\contentsline {chapter}{\numberline {4}Algorytm detekcji d\IeC {\l }oni}{16}{chapter.4}% 
\contentsline {section}{\numberline {4.1}Przechwycenie obrazu z kamery}{18}{section.4.1}% 
\contentsline {section}{\numberline {4.2}Ustawienie t\IeC {\l }a}{18}{section.4.2}% 
\contentsline {section}{\numberline {4.3}Wykrycie twarzy u\IeC {\.z}ytkownika}{20}{section.4.3}% 
\contentsline {section}{\numberline {4.4}Segmentacja obrazu wraz z binaryzacj\IeC {\k a}}{23}{section.4.4}% 
\contentsline {section}{\numberline {4.5}Wykrycie najwi\IeC {\k e}kszego konturu}{25}{section.4.5}% 
\contentsline {section}{\numberline {4.6}Utworzenie regionu zainteresowania}{27}{section.4.6}% 
\contentsline {section}{\numberline {4.7}Wys\IeC {\l }anie obrazu do klasyfikacji}{27}{section.4.7}% 
\contentsline {chapter}{\numberline {5}Rozpoznawanie gest\IeC {\'o}w d\IeC {\l }oni}{28}{chapter.5}% 
\contentsline {section}{\numberline {5.1}Zbi\IeC {\'o}r danych}{28}{section.5.1}% 
\contentsline {section}{\numberline {5.2}Model sieci neuronowej}{29}{section.5.2}% 
\contentsline {subsection}{\numberline {5.2.1}Architektura}{29}{subsection.5.2.1}% 
\contentsline {subsection}{\numberline {5.2.2}Trening modelu}{31}{subsection.5.2.2}% 
\contentsline {section}{\numberline {5.3}Biblioteka umo\IeC {\.z}liwiaj\IeC {\k a}ca rozpoznawanie - ImageClassificationTF}{31}{section.5.3}% 
\contentsline {chapter}{\numberline {6}Testy}{35}{chapter.6}% 
\contentsline {section}{\numberline {6.1}Algorytm detekcji d\IeC {\l }oni}{35}{section.6.1}% 
\contentsline {section}{\numberline {6.2}Sie\IeC {\'c} neuronowa}{36}{section.6.2}% 
\contentsline {chapter}{\numberline {7}Zako\IeC {\'n}czenie}{39}{chapter.7}% 

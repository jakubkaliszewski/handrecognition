﻿using System.Drawing;

namespace DrawingObjectsLibrary
{
    public sealed class CircleObject : DrawingObject
    {
        public System.Drawing.Drawing2D.GraphicsPath ElipsePath { get; private set; }

        public CircleObject()
        {
            ///Rectangle = new Rectangle();
            //var path = new System.Drawing.Drawing2D.GraphicsPath();
            //path.AddEllipse(0, 0, label1.Width, label1.Height);
        }
        
        //kolor
        //rozmiar->szerokosc, wysokosc
        //lokalizacja - jedna, do rectangle

        public override Color Color { get; set; }
        public override Point Point { get; set; }
        public override Size Size { get; set; }

        public override void Draw(Bitmap bitmap)
        {
            throw new System.NotImplementedException();
        }
    }
}
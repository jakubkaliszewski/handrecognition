using System.Drawing;
using System.Drawing.Imaging;

namespace DrawingObjectsLibrary
{
    public abstract class DrawingObject
    {
        public abstract Color Color { get; set; }
        public abstract Point Point { get; set; }
        public abstract Size Size { get; set; }
        public abstract void Draw(Bitmap bitmap);


    }
}
using System.IO;
using System.Linq;

namespace ImageClassificationTF
{
    public static class RecognizerHelpers
    {
        static FileInfo _dataRoot = new FileInfo(typeof(Recognizer).Assembly.Location);
        public static string GetAssetsPath(params string[] paths)
        {
            if (paths == null || paths.Length == 0)
                return null;

            return Path.Combine(_dataRoot.Directory.FullName + paths[0]);
        }
    }
}
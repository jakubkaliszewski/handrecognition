﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Emgu.CV;
using Emgu.TF;

namespace Emgu.TF
{
    public static class TensorConvert
    {
        /// <summary>
        /// Conver a 1 channel BGR Mat to a Tensor
        /// </summary>
        /// <param name="image">The input Emgu CV Mat</param>
        /// <param name="inputHeight">The height of the image in the output tensor, if it is -1, the height will not be changed.</param>
        /// <param name="inputWidth">The width of the image in the output tensor, if it is -1, the width will not be changed.</param>
        /// <param name="inputMean">The mean, if it is not 0, the value will be substracted from the pixel values</param>
        /// <param name="scale">The optional scale</param>
        /// <param name="dataType">The type of the data in the tensor</param>
        /// <param name="status">The tensorflow status</param>
        /// <returns>The tensorflow tensor</returns>
        public static Tensor ReadTensorFromMatGray(Mat image, DataType dataType, int inputHeight = -1, int inputWidth = -1, float inputMean = 0.0f, float scale = 1.0f, Status status = null)
        {
            if (image.NumberOfChannels != 1)
            {
                throw new ArgumentException("Input must be 1 channel Gray image");
            }

            Emgu.CV.CvEnum.DepthType depth = image.Depth;
            if (!(depth == Emgu.CV.CvEnum.DepthType.Cv8U || depth == Emgu.CV.CvEnum.DepthType.Cv32F))
            {
                throw new ArgumentException("Input image must be 8U or 32F");
            }

            //resize
            int finalHeight = inputHeight == -1 ? image.Height : inputHeight;
            int finalWidth = inputWidth == -1 ? image.Width : inputWidth;
            Size finalSize = new Size(finalWidth, finalHeight);

            if (image.Size != finalSize)
            {
                using (Mat tmp = new Mat())
                {
                    CvInvoke.Resize(image, tmp, finalSize);
                    return ReadTensorFromMatGray(tmp, inputMean, scale, dataType);
                }
            }
            else
            {
                return ReadTensorFromMatGray(image, inputMean, scale, dataType);
            }
        }

        private static Tensor ReadTensorFromMatGray(Mat image, float inputMean, float scale, DataType type)
        {
            if (type == DataType.Float)
            {
                Tensor t = new Tensor(type, new int[] { 1, image.Height, image.Width, 1 });
                using (Mat matF = new Mat(image.Size, Emgu.CV.CvEnum.DepthType.Cv32F, 1, t.DataPointer, sizeof(float) * 1 * image.Width))
                {
                    image.ConvertTo(matF, Emgu.CV.CvEnum.DepthType.Cv32F, scale, -inputMean * scale);
                }
                return t;
            }
            else if (type == DataType.Uint8)
            {
                Tensor tensor = new Tensor(type, new int[] { 1, image.Height, image.Width, 1 });

                using (Mat matB = new Mat(image.Size, Emgu.CV.CvEnum.DepthType.Cv8U, 1, tensor.DataPointer, sizeof(byte) * 1 * image.Width))
                {
                    if (scale == 1.0f && inputMean == 0)
                    {
                        image.CopyTo(matB);
                    }
                    else
                        CvInvoke.ConvertScaleAbs(image, matB, scale, -inputMean * scale);
                }
                return tensor;

            }
            else
            {
                throw new Exception($"Data Type of {type} is not supported.");
            }
        }

    }
}

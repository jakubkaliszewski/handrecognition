namespace ImageClassificationTF.Structures
{
    public struct InceptionSettings
    {
        // for checking tensor names, you can use tools like Netron,
        // which is installed by Visual Studio AI Tools

        // input tensor name
        public const string inputTensorName = "conv2d_1_input";

        // output tensor name
        public const string outputTensorName = "dense_2/Softmax";
    }
}
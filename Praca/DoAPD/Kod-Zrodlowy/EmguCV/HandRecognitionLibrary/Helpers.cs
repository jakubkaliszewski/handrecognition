﻿using System;
using System.Threading;
using HandRecognitionLibrary.Models;
using HandRecognitionLibrary.Resources;

namespace HandRecognitionLibrary
{
    public class Helpers
    {
        public const int ImageWidth = 500, ImageHeight = 375; //globalnie widoczne rozmiary klatki
        private const int _interval = 60;
        public Camera Camera { get; private set; }
        public HandBlob HandBlob { get; private set; }
        public HandGestureDetector HandGestureDetector { get; private set; }
        public CameraRoiPreview CameraRoiPreview { get; private set; }

        private Thread cameraPreviewThread;
        private Thread cameraRoiPreviewThread;

        public Helpers()
        {
            Camera = new Camera();
            HandBlob = new HandBlob();
            HandGestureDetector = new HandGestureDetector();
        }

        public void CamerasInitialization()
        {
            CameraPreview cameraPreview = new CameraPreview(Strings.CameraPreview);
            CameraRoiPreview = new CameraRoiPreview(Strings.ROIPreview);

            Camera.CapturedFrameFromCamera += cameraPreview.OnCapturedFrameFromCamera;
            Camera.CapturedFrameFromCamera += HandBlob.OnCapturedFrameFromCamera;

            HandBlob.CreatedBlob += CameraRoiPreview.OnCreatedContours;
            HandBlob.HandContours.ContoursExist += HandGestureDetector.HandContoursDetectorOnContoursExist;


            cameraPreviewThread = new Thread(new ThreadStart(cameraPreview.StartPreview));
            cameraRoiPreviewThread = new Thread(new ThreadStart(CameraRoiPreview.StartPreview));

            System.Timers.Timer
                frameTimer =
                    new System.Timers.Timer(
                        _interval); //start timer to query webcam image, as well as read gestures once every 40ms
            frameTimer.Elapsed += QueryWebcamImageOnTickTimer;
            frameTimer.Enabled = true;

            cameraPreviewThread.Start();
            cameraRoiPreviewThread.Start();
        }

        public void Dispose()
        {
            cameraPreviewThread.Abort();
            cameraRoiPreviewThread.Abort();
        }

        private void QueryWebcamImageOnTickTimer(object sender, EventArgs e)
        {
            //Wyślij klatkę z kamery do przetwarzania oraz na podgląd surową
            Camera.SetSettings();
            Camera.TakeFrame();
        }
    }
}
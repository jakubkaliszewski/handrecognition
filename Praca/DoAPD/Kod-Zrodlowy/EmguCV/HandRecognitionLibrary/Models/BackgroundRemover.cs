using System;
using Emgu.CV;
using Emgu.CV.CvEnum;

namespace HandRecognitionLibrary.Models
{
    public class BackgroundRemover
    {
        private Mat _background;
        private bool _calibrated;
        private Mat Input { get; set; }

        public void Calibrate(Mat input)
        {
            Input = input;
            _background = new Mat();
            CvInvoke.CvtColor(Input, _background, ColorConversion.Bgr2Gray); //czyli tło jest szare
            _calibrated = true;
        }

        public Mat GetForeground(Mat input)
        {
            Input = input;//w BGR
            Mat foregroundMask = GetForegroundMask();

            Mat foreground = new Mat();
            Input.CopyTo(foreground, foregroundMask);

            return foreground;
        }

        private Mat GetForegroundMask()
        {
            Mat foregroundMask;

            if (!_calibrated)
            {
                foregroundMask = Mat.Zeros(Input.Rows, Input.Cols, DepthType.Cv8U, Input.NumberOfChannels);
                return foregroundMask;
            }

            foregroundMask = new Mat();
            CvInvoke.CvtColor(Input, foregroundMask,
                ColorConversion.Bgr2Gray);//foreground w odcieniach szarości

            return RemoveBackground(foregroundMask);
        }

        private Mat RemoveBackground(Mat foregroundMask)
        {
            int thresholdOffset = 10;

            Matrix<Byte> foregroundMatrix =
                new Matrix<Byte>(foregroundMask.Rows, foregroundMask.Cols, foregroundMask.NumberOfChannels);
            foregroundMask.CopyTo(foregroundMatrix);

            Matrix<Byte> backgroundMatrix =
                new Matrix<Byte>(_background.Rows, _background.Cols, _background.NumberOfChannels);
            _background.CopyTo(backgroundMatrix);

            for (int i = 0; i < foregroundMask.Rows; i++)
            {
                for (int j = 0; j < foregroundMask.Cols; j++)
                {
                    var framePixel = Convert.ToInt32(foregroundMatrix.Data.GetValue(i, j));
                    var backgroundPixel = Convert.ToInt32(backgroundMatrix.Data.GetValue(i, j));

                    if (framePixel >= backgroundPixel - thresholdOffset &&
                        framePixel <= backgroundPixel + thresholdOffset)
                    {
                        foregroundMatrix.Data.SetValue(byte.MinValue, i, j);
                    }
                    else
                    {
                        foregroundMatrix.Data.SetValue(byte.MaxValue, i, j);
                    }
                }
            }

            return foregroundMatrix.Mat;
        }
    }
}

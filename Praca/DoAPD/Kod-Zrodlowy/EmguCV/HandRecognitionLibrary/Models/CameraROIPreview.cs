﻿namespace HandRecognitionLibrary.Models
{
    public class CameraRoiPreview : CameraPreview
    {
        public void OnCreatedContours(object sender, HandBlobFromCameraEventArgs e)
        {
            Viewer.Image = e.Blob;
        }

        public CameraRoiPreview(string windowName) : base(windowName)
        {
        }
    }
}
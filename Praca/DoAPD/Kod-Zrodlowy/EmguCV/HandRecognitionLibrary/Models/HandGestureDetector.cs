﻿using ImageClassificationTF;
using System;
using System.Drawing;
using Emgu.CV;


namespace HandRecognitionLibrary.Models
{
    public class HandGestureDetector
    {
        private readonly Recognizer Recognizer;
        private Mat _image;
        private static string _lastImageLabel;
        public event EventHandler<PredictionEventArgs> CapturedPrediction;

        public event EventHandler<GestureEventArgs> CapturedGesture;

        //publisher
        protected virtual void OnCapturedPrediction(string prediction)
        {
            if (CapturedPrediction != null)
                CapturedPrediction(null, new PredictionEventArgs() {Prediction = prediction});
        }

        protected virtual void OnCapturedGesture(bool status)
        {
            if (CapturedGesture != null)
                CapturedGesture(null, new GestureEventArgs() {Status = status});
        }

        public HandGestureDetector()
        {
            Recognizer = new Recognizer();
            _lastImageLabel = null;
        }

        private string GetImagePrediction()
        {
            if (_image != null)
            {
                var results = Recognizer.Recognize(_image);
                if (results.value > .80)
                {
                    SendImagePredictions(results.label);
                    return results.label;
                }
            }

            SendImagePredictions("Nie rozpoznano");
            return String.Empty;
        }

        private void SendImagePredictions(string prediction)
        {
            OnCapturedPrediction(prediction);
        }

        public void SendInfoAboutGesture(bool status)
        {
            OnCapturedGesture(status);
        }

        public bool IsGesture(Mat image)
        {
            _image = image;
            var resultLabel = GetImagePrediction();

            if (resultLabel == String.Empty || resultLabel == _lastImageLabel || resultLabel == "hand")
                return false;

            if (_lastImageLabel == null)
            {
                _lastImageLabel = resultLabel;
                return false;
            }

            if (resultLabel != "hand")
            {
                _lastImageLabel = resultLabel;
                return true;
            }

            _lastImageLabel = resultLabel;
            return false;
        }

        public void HandContoursDetectorOnContoursExist(object sender, ContoursEventArgs e)
        {
            SendInfoAboutGesture(IsGesture(e.Contours));
        }
    }

    public class GestureEventArgs : EventArgs
    {
        public bool Status { get; set; }
    }

    public class PredictionEventArgs : EventArgs
    {
        public string Prediction { get; set; }
    }
}
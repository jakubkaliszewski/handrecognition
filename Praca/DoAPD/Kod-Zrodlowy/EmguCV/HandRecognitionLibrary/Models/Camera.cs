﻿using System;
using Emgu.CV;

namespace HandRecognitionLibrary.Models
{
    public class Camera
    {
        private VideoCapture _camera;
        public static event EventHandler<CameraEventArgs> CapturedFrameFromCamera;
        public int CameraNumber { get; private set; }

        //publisher
        protected virtual void OnCapturedFrameFromCamera(Mat newImage)
        {
            if (CapturedFrameFromCamera != null)
                CapturedFrameFromCamera(this, new CameraEventArgs() {Image = newImage});
        }

        public void SetSettings()
        {
            _camera.SetCaptureProperty(Emgu.CV.CvEnum.CapProp.Exposure, 0.1);
        }

        public Camera(int cameraNumber = 0) //znaczy domyślna wbudowana
        {
            CameraNumber = cameraNumber;
            _camera = new VideoCapture(CameraNumber);
            try
            {
                _camera.SetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameWidth, Helpers.ImageWidth);
                _camera.SetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameHeight, Helpers.ImageHeight);
                _camera.SetCaptureProperty(Emgu.CV.CvEnum.CapProp.AutoExposure, 0.25);
                _camera.SetCaptureProperty(Emgu.CV.CvEnum.CapProp.IsoSpeed, 100);
                _camera.SetCaptureProperty(Emgu.CV.CvEnum.CapProp.Exposure, 0.01);
            }
            catch (Exception)
            {
                throw new Exception("Błąd ustawiania rozmiaru ramki kamery!!");
            }
        }

        public void TakeFrame()
        {
            OnCapturedFrameFromCamera(_camera.QueryFrame()); //Przechwycenie klatki
        }
    }

    public class CameraEventArgs : EventArgs
    {
        public Mat Image { get; set; }
    }
}
﻿using System;
using System.Drawing;
using System.Text;
using System.Threading;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using HandRecognitionLibrary.Structures;

namespace HandRecognitionLibrary.Models
{
    public class HandContoursDetector
    {
        private Mat _handMask;
        private VectorOfVectorOfPoint _contours;
        private Image<Bgr, Byte> contoursImage;
        private int _biggestContourIndex;
        private Rectangle _boundingRectangle;
        private Thread _contoursExistEventThread;
        private Mat _copyOfImageContours;

        public bool ContoursExistBool { get; private set; }
        public Mat ImageContours { get; private set; }
        public event EventHandler<ContoursEventArgs> ContoursExist;


        //publisher
        protected virtual void OnContoursExist()
        {
            if (ContoursExist != null)
                ContoursExist(this, new ContoursEventArgs() {Contours = ResizeMatInGrayWithContours()});
        }

        public Mat DeterminateAndDrawHandContours(Mat handMask)
        {
            _handMask = handMask;
            _copyOfImageContours = Mat.Zeros(_handMask.Rows, _handMask.Cols, DepthType.Cv8U, 3);
            ImageContours = Mat.Zeros(_handMask.Rows, _handMask.Cols, DepthType.Cv8U, _handMask.NumberOfChannels);

            if (!IsGoodInputData())
            {
                ContoursExistBool = false;
                return ImageContours;
            }

            DeterminateHandContours();
            if (ContoursExistBool)
            {
                DrawHandContours();
                DrawRectangle();

                ImageContours.CopyTo(_copyOfImageContours);

                _contoursExistEventThread = new Thread(new ThreadStart(OnContoursExist));
                _contoursExistEventThread.Start();
            }

            return ImageContours;
        }

        public void SaveRectangleWithHandContours()
        {
            if (ContoursExistBool)
            {
                DateTime date = DateTime.Now;
                StringBuilder sb = new StringBuilder();
                sb.AppendFormat("{0}-{1}_{2}-{3}-{4}.png", date.Day, date.Month, date.Hour, date.Minute, date.Second);
                string path = sb.ToString();

                try
                {
                    PrepareMatToSave().Bitmap.Save(path);
                }
                catch (Exception e)
                {
                    throw new Exception($"Saving image problem: {e}");
                }
            }
        }

        private Mat ResizeMatInGrayWithContours()
        {
            var resizedMat = new Mat(_copyOfImageContours, _boundingRectangle);
            CvInvoke.Resize(resizedMat, resizedMat, new Size(48, 48), interpolation: Inter.Lanczos4);
            resizedMat = resizedMat.ToImage<Gray, float>().Mat;
            return resizedMat;
        }

        private Mat PrepareMatToSave()
        {
            var resizedMat = new Mat(ImageContours, _boundingRectangle);
            CvInvoke.Resize(resizedMat, resizedMat, new Size(48, 48), interpolation: Inter.Lanczos4);
            CvInvoke.CvtColor(resizedMat, resizedMat, ColorConversion.Bgr2Gray);
            return resizedMat;
        }


        private bool IsGoodInputData()
        {
            if (_handMask.IsEmpty || _handMask.NumberOfChannels != 1)
                return false;

            return true;
        }

        private void DeterminateHandContours()
        {
            _contours = new VectorOfVectorOfPoint();
            CvInvoke.FindContours(_handMask, _contours, null, RetrType.External,
                ChainApproxMethod.ChainApproxTc89L1);

            // Potrzebujemy jakichkolwiek konturow do kontynuacji pracy
            if (_contours.Size <= 0)
            {
                ContoursExistBool = false;
                return;
            }
            // Znajduje najwiekszy kontur -> powinna być to ręka
            _biggestContourIndex = 0;
            double localBiggestArea = 0.0;

            for (int i = 0; i < _contours.Size; i++)
            {
                var localContourArea = CvInvoke.ContourArea(_contours[i], false);

                if (localContourArea > localBiggestArea)
                {
                    localBiggestArea = localContourArea;
                    _biggestContourIndex = i;
                }
            }

            ContoursExistBool = true;
        }

        private void DrawHandContours()
        {
            contoursImage = new Image<Bgr, byte>(ImageContours.Bitmap);

            contoursImage.Draw(_contours, _biggestContourIndex, new Bgr(Color.Chartreuse), 2);
            ImageContours = contoursImage.Mat;
        }

        private void DrawRectangle()
        {
            _boundingRectangle = CvInvoke.BoundingRectangle(_contours[_biggestContourIndex]);
            CvInvoke.Rectangle(ImageContours, _boundingRectangle, ColorsInScalar.RedColor, 2);
        }
    }

    public class ContoursEventArgs
    {
        public Mat Contours { get; set; }
    }
}

﻿using System;
using System.Threading;
using Emgu.CV;

namespace HandRecognitionLibrary.Models
{
    public class HandBlob
    {
        public event EventHandler<HandBlobFromCameraEventArgs> CreatedBlob;

        //publisher
        protected virtual void OnCreatedBlob(Mat newImage)
        {
            if (CreatedBlob != null)
                CreatedBlob(this, new HandBlobFromCameraEventArgs() {Blob = newImage});
        }

        private BackgroundRemover _backgroundRemover;
        private SkinDetector _skinDetector;
        private FaceRemover _faceDetector;
        public HandContoursDetector HandContours { get; set; }
        private bool _calibrationIsInProgress = false;

        public HandBlob()
        {
            _backgroundRemover = new BackgroundRemover();
            _skinDetector = new SkinDetector();
            _faceDetector = new FaceRemover();
            HandContours = new HandContoursDetector();
        }

        private void SetBackground(Mat image)
        {
            _backgroundRemover.Calibrate(image);
            _calibrationIsInProgress = false;
        }

        private void SetSkinColorFromSamples(Mat image)
        {
            _skinDetector.Calibrate(image);
            _calibrationIsInProgress = false;
        }

        private void GetHandBlob(Mat image)
        {
            var clonedImage = new Mat();
            image.CopyTo(clonedImage);

            var foreground = _backgroundRemover.GetForeground(clonedImage);
            _faceDetector.PrepareFaceRectangles(clonedImage);
            _skinDetector.CalibrateFromFace(clonedImage, _faceDetector.FaceRectangles);
            _faceDetector.RemoveFaces(clonedImage, foreground);
            var handMask = _skinDetector.GetSkinMask(foreground);
            var contours = HandContours.DeterminateAndDrawHandContours(handMask);
            //handMask;

            OnCreatedBlob(contours);
        }

        public void OnCapturedFrameFromCamera(object sender, CameraEventArgs e)
        {
            GetHandBlob(e.Image);
        }

        public void OnSetSkinColorRequest(object sender, CameraEventArgs e)
        {
            if (_calibrationIsInProgress)
                return;
            _calibrationIsInProgress = true;
            Camera.CapturedFrameFromCamera -= OnSetSkinColorRequest;
            new Thread(new ThreadStart(new Action(() => { SetSkinColorFromSamples(e.Image); }))).Start();
        }

        public void OnBackgroundRequest(object sender, CameraEventArgs e)
        {
            if (_calibrationIsInProgress)
                return;
            _calibrationIsInProgress = true;
            Camera.CapturedFrameFromCamera -= OnBackgroundRequest;
            new Thread(new ThreadStart(new Action(() => { SetBackground(e.Image); }))).Start();
        }

        public void OnSaveActualContoursRequest(object sender, CameraEventArgs e)
        {
            Camera.CapturedFrameFromCamera -= OnSaveActualContoursRequest;
            HandContours.SaveRectangleWithHandContours();
        }
    }

    public class HandBlobFromCameraEventArgs : EventArgs
    {
        public Mat Blob { get; set; }
    }
}
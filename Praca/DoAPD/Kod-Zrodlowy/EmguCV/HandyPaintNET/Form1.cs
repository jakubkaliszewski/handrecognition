﻿using System;
using System.Threading;
using System.Windows.Forms;
using HandRecognitionLibrary;
using HandRecognitionLibrary.Models;

namespace HandyPaintNET
{
    public partial class Form1 : Form
    {
        private Helpers cameraHelper;
        private Thread cameraHelperThread;

        public Form1()
        {
            InitializeComponent();
            cameraHelper = new Helpers();
            cameraHelperThread = new Thread(new ThreadStart(cameraHelper.CamerasInitialization));
            cameraHelperThread.Start();

            cameraHelper.HandBlob.HandContours.ContoursExist += cameraHelper.HandGestureDetector.HandContoursDetectorOnContoursExist;
            cameraHelper.HandGestureDetector.CapturedPrediction += HandGestureDetectorOnCapturedPrediction;

            //HandGestureDetector.CapturedGesture += HandGestureDetectorOnCapturedGesture;
        }

        private void HandGestureDetectorOnCapturedGesture(object sender, GestureEventArgs e)
        {
            GestureValue.Text = e.Status ? "Tak" : "Nie";
        }

        private void HandGestureDetectorOnCapturedPrediction(object sender, PredictionEventArgs e)
        {
            ChangePredictionLabel(e.Prediction);
        }

        private void ChangePredictionLabel(string str)
        {
            if (predictionLabel.InvokeRequired)
                predictionLabel.Invoke(new Action<string>(ChangePredictionLabel), str);
            else
            {
                predictionLabel.Text = str;
            }
        }

        private void setBackgroundButton_Click(object sender, EventArgs e)
        {
            Camera.CapturedFrameFromCamera += cameraHelper.HandBlob.OnBackgroundRequest;
        }

        private void saveActualContoursFrameButton_Click(object sender, EventArgs e)
        {
            Camera.CapturedFrameFromCamera += cameraHelper.HandBlob.OnSaveActualContoursRequest;
        }
    }
}

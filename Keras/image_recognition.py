# encoding: utf-8
# author: Jakub Kaliszewski (kontakt@jakubkaliszewski.pl)
# works on python 2.7.x

# Importing the Keras libraries and packages
from keras.utils.np_utils import to_categorical
from tensorflow.python.platform import gfile
import tensorflow as tf
from keras import backend as K
import keras
from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten, Conv2D, MaxPooling2D
import matplotlib.pyplot as plt
import os as os

wkdir = os.getcwd()
pb_filename = 'model.pb'

num_classes = 3
image_rows, image_cols = 48, 48
input_shape = (image_rows, image_cols, 1)
batch_size = 100

#Load dataset

train_datagen = ImageDataGenerator()
test_datagen = ImageDataGenerator()

training_set = train_datagen.flow_from_directory('Dataset/training_set',
                                                 target_size=(image_rows, image_rows),
                                                 batch_size=batch_size,
                                                 class_mode='categorical',
                                                 color_mode='grayscale',
                                                 shuffle=True
                                                 )

test_set = test_datagen.flow_from_directory('Dataset/test_set',
                                            target_size=(image_rows, image_rows),
                                            batch_size=20,
                                            class_mode='categorical',
                                            color_mode='grayscale',
                                            shuffle=True)

#training_set = to_categorical(training_set)
#test_set = to_categorical(test_set)

#Model

model = Sequential([Conv2D(filters=4, kernel_size=(7, 7), activation='relu', input_shape=input_shape),
                    Conv2D(filters=8, kernel_size=(5, 5), activation='relu', input_shape=(48,48,1)),
                    MaxPooling2D(pool_size=(2, 2)),
                    Dropout(0.5),
                    Conv2D(filters=16, kernel_size=(5, 5), activation='relu', input_shape=(24,24,1)),
                    Conv2D(filters=32, kernel_size=(3, 3), activation='relu', input_shape=(24,24,1)),
                    MaxPooling2D(pool_size=(2, 2)),
                    Dropout(0.5),
                    Conv2D(filters=64, kernel_size=(3, 3), activation='relu', input_shape=(12, 12, 1)),
                    Conv2D(filters=128, kernel_size=(3, 3), activation='relu', input_shape=(12, 12, 1)),
                    MaxPooling2D(pool_size=(2, 2)),
                    Dropout(0.5),
                    Flatten(),
                    Dense(256, activation='relu'),
                    Dropout(0.5),
                    Dense(3, activation='softmax')])

model.compile(metrics=['accuracy'],
            loss=keras.losses.categorical_crossentropy,
            optimizer='adam')

history = model.fit_generator(training_set,
                    epochs=120,
                    steps_per_epoch=100, 
                    validation_data=test_set,
                    validation_steps=14)



# Plot training & validation accuracy values
plt.plot(history.history['acc'])
plt.plot(history.history['val_acc'])
plt.title('Model accuracy')
plt.ylabel('Accuracy')
plt.xlabel('Epoch')
plt.legend(['Train', 'Test'], loc='upper left')
plt.show()

# Plot training & validation loss values
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('Model loss')
plt.ylabel('Loss')
plt.xlabel('Epoch')
plt.legend(['Train', 'Test'], loc='upper left')
plt.show()


# save model to pb ====================
def freeze_session(session, keep_var_names=None, output_names=None, clear_devices=True):
    """
    Freezes the state of a session into a pruned computation graph.
    Creates a new computation graph where variable nodes are replaced by
    constants taking their current value in the session. The new graph will be
    pruned so subgraphs that are not necessary to compute the requested
    outputs are removed.
    @param session The TensorFlow session to be frozen.
    @param keep_var_names A list of variable names that should not be frozen,
                          or None to freeze all the variables in the graph.
    @param output_names Names of the relevant graph outputs.
    @param clear_devices Remove the device directives from the graph for better portability.
    @return The frozen graph definition.
    """
    from tensorflow.python.framework.graph_util import convert_variables_to_constants
    graph = session.graph
    with graph.as_default():
        freeze_var_names = list(
            set(v.op.name for v in tf.global_variables()).difference(keep_var_names or []))
        output_names = output_names or []
        output_names += [v.op.name for v in tf.global_variables()]
        input_graph_def = graph.as_graph_def()
        if clear_devices:
            for node in input_graph_def.node:
                node.device = ""
        frozen_graph = convert_variables_to_constants(session, input_graph_def,
                                                      output_names, freeze_var_names)
        return frozen_graph


# save keras model as tf pb files ===============
frozen_graph = freeze_session(K.get_session(),
                              output_names=[out.op.name for out in model.outputs])
tf.train.write_graph(frozen_graph, wkdir, pb_filename, as_text=False)


# # load & inference the model ==================

with tf.Session() as sess:
    # load model from pb file
    with gfile.FastGFile(wkdir+'/'+pb_filename, 'rb') as f:
        graph_def = tf.GraphDef()
        graph_def.ParseFromString(f.read())
        sess.graph.as_default()
        g_in = tf.import_graph_def(graph_def)
    # write to tensorboard (check tensorboard for each op names)
    writer = tf.summary.FileWriter(wkdir+'/log/')
    writer.add_graph(sess.graph)
    writer.flush()
    writer.close()
    # print all operation names
    print('\n===== ouptut operation names =====\n')
    for op in sess.graph.get_operations():
      print(op)
    # inference by the model (op name must comes with :0 to specify the index of its output)
    tensor_output = sess.graph.get_tensor_by_name(
        'import/dense_2/Softmax:0')
    tensor_input = sess.graph.get_tensor_by_name('import/conv2d_1_input:0')

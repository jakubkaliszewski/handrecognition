# encoding: utf-8
# author: Jakub Kaliszewski (kontakt@jakubkaliszewski.pl)
# works on python 3.6.x

# Part 1 - Building the CNN

# Importing the Keras libraries and packages
from tensorflow.python.platform import gfile
import tensorflow as tf
from keras import backend as K
from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential
from keras.layers import Convolution2D
from keras.layers import MaxPooling2D
from keras.layers import Flatten
from keras.layers import Dense


# parameter ==========================
wkdir = '/media/DaneDzielone/Repozytoria/handrecognition/Keras'
pb_filename = 'model.pb'

# Initialising the CNN
model = Sequential()

# First convolutional layer
model.add(Convolution2D(
    31, 5, 5, input_shape=(48, 48, 1), activation='relu', bias_initializer='zeros', use_bias=True))
model.add(MaxPooling2D(pool_size=(8, 8)))

# Second convolutional layer
model.add(Convolution2D(15, 3, 3, activation='relu', use_bias=True))
model.add(MaxPooling2D(pool_size=(3, 3)))

# Third convolutional layer
model.add(Convolution2D(7, 1, 1,activation='relu', use_bias=True))
model.add(MaxPooling2D(pool_size=(1, 1)))

# Step 3 - Flattening
model.add(Flatten())

# Step 4 - Full connection
model.add(Dense(output_dim=128, activation='relu', use_bias=True))
model.add(Dense(output_dim=1, activation='softmax'))

# Compiling the CNN
model.compile(
    optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])

# Part 2 - Fitting the CNN to the images


train_datagen = ImageDataGenerator(rescale=1./255,
                                   shear_range=0.2,
                                   zoom_range=0.2,
                                   horizontal_flip=True)

test_datagen = ImageDataGenerator(rescale=1./255)

training_set = train_datagen.flow_from_directory('Dataset/training_set',
                                                 target_size=(48, 48),
                                                 batch_size=32,
                                                 class_mode='binary',
                                                 color_mode='grayscale')

test_set = test_datagen.flow_from_directory('Dataset/test_set',
                                            target_size=(48, 48),
                                            batch_size=32,
                                            class_mode='binary',
                                            color_mode='grayscale')

model.fit_generator(training_set,
                         samples_per_epoch=1000,
                         nb_epoch=5000,
                         validation_data=test_set,
                         nb_val_samples=100)


# save model to pb ====================
def freeze_session(session, keep_var_names=None, output_names=None, clear_devices=True):
    """
    Freezes the state of a session into a pruned computation graph.
    Creates a new computation graph where variable nodes are replaced by
    constants taking their current value in the session. The new graph will be
    pruned so subgraphs that are not necessary to compute the requested
    outputs are removed.
    @param session The TensorFlow session to be frozen.
    @param keep_var_names A list of variable names that should not be frozen,
                          or None to freeze all the variables in the graph.
    @param output_names Names of the relevant graph outputs.
    @param clear_devices Remove the device directives from the graph for better portability.
    @return The frozen graph definition.
    """
    from tensorflow.python.framework.graph_util import convert_variables_to_constants
    graph = session.graph
    with graph.as_default():
        freeze_var_names = list(
            set(v.op.name for v in tf.global_variables()).difference(keep_var_names or []))
        output_names = output_names or []
        output_names += [v.op.name for v in tf.global_variables()]
        input_graph_def = graph.as_graph_def()
        if clear_devices:
            for node in input_graph_def.node:
                node.device = ""
        frozen_graph = convert_variables_to_constants(session, input_graph_def,
                                                      output_names, freeze_var_names)
        return frozen_graph


# save keras model as tf pb files ===============
frozen_graph = freeze_session(K.get_session(),
                              output_names=[out.op.name for out in model.outputs])
tf.train.write_graph(frozen_graph, wkdir, pb_filename, as_text=False)


# # load & inference the model ==================

with tf.Session() as sess:
    # load model from pb file
    with gfile.FastGFile(wkdir+'/'+pb_filename, 'rb') as f:
        graph_def = tf.GraphDef()
        graph_def.ParseFromString(f.read())
        sess.graph.as_default()
        g_in = tf.import_graph_def(graph_def)
    # write to tensorboard (check tensorboard for each op names)
    writer = tf.summary.FileWriter(wkdir+'/log/')
    writer.add_graph(sess.graph)
    writer.flush()
    writer.close()
    # print all operation names
    print('\n===== ouptut operation names =====\n')
    for op in sess.graph.get_operations():
      print(op)
    # inference by the model (op name must comes with :0 to specify the index of its output)
    tensor_output = sess.graph.get_tensor_by_name('import/dense_3/Softmax:0')
    tensor_input = sess.graph.get_tensor_by_name('import/dense_1_input:0')
    predictions = sess.run(tensor_output, {tensor_input: x})
    print('\n===== output predicted results =====\n')
print(predictions)
                         

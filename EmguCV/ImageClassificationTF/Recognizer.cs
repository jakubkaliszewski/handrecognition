using System;
using System.IO;
using Emgu.CV;
using Emgu.TF;
using Emgu.TF.Models.Own;
using ImageClassificationTF.Structures;

namespace ImageClassificationTF
{
    public class Recognizer
    {
        private Inception _inceptionGraph;
        private string _assetsPath = RecognizerHelpers.GetAssetsPath(@"\Assets");//Ścieżka do sprawdzenia
        private Tensor _imageTensor;

        public Recognizer()
        {
            _inceptionGraph = new Inception();

            string inceptionPb = Path.Combine(_assetsPath, "model.pb");
            string labelsTxt = Path.Combine(_assetsPath, "output_labels.txt");
            _inceptionGraph.Init(inceptionPb, labelsTxt, InceptionSettings.inputTensorName, InceptionSettings.outputTensorName);
        }

        public Tuple<string,double> Recognize(Mat image)
        {
            _imageTensor = TensorConvert.ReadTensorFromMatGray(image, DataType.Float);
            if (_inceptionGraph.Imported)
            {
                var vqa = _inceptionGraph.MostLikely(_imageTensor);

                if (vqa != null)
                    return new Tuple<string, double>(vqa.Label, vqa.Probability); 
                
            }

            return new Tuple<string, double>(string.Empty, float.NaN);
        }



    }
}

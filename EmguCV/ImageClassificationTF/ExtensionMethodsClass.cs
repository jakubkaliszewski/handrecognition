using System.Drawing;
using Emgu.TF;
using Emgu.TF.Models;

namespace ImageClassificationTF
{
    public static class ExtensionMethodsClass
    {
         public static byte[] ToByteArray(this Image image)
         {
             ImageConverter converter = new ImageConverter();
             return (byte[])converter.ConvertTo(image, typeof(byte[]));
         }

        //zapożyczony kod źródłowy EmguTF
        public static Tensor ReadTensorFromBitmap(
             this Bitmap image,
             int inputHeight = -1, 
             int inputWidth = -1, 
             float inputMean = 0.0f, 
             float scale = 1.0f,
             Status status = null)
         {

             using (StatusChecker checker = new StatusChecker(status))
             {
                 var graph = new Graph();
                 Operation input = graph.Placeholder(DataType.String);

                 Operation jpegDecoder = graph.DecodeJpeg(input, 1); //dimension 3

                 Operation floatCaster = graph.Cast(jpegDecoder, DstT: DataType.Float); //cast to float

                 Tensor axis = new Tensor(0);
                 Operation axisOp = graph.Const(axis, axis.Type, opName: "axis");
                 Operation dimsExpander = graph.ExpandDims(floatCaster, axisOp); //turn it to dimension [1,3]

                 Operation resized;
                 bool resizeRequired = (inputHeight > 0) && (inputWidth > 0);
                 if (resizeRequired)
                 {
                     Tensor size = new Tensor(new int[] {inputHeight, inputWidth}); // new size;
                     Operation sizeOp = graph.Const(size, size.Type, opName: "size");
                     resized = graph.ResizeBilinear(dimsExpander, sizeOp); //resize image
                 }
                 else
                 {
                     resized = dimsExpander;
                 }

                 Tensor mean = new Tensor(inputMean);
                 Operation meanOp = graph.Const(mean, mean.Type, opName: "mean");
                 Operation substracted = graph.Sub(resized, meanOp);

                 Tensor scaleTensor = new Tensor(scale);
                 Operation scaleOp = graph.Const(scaleTensor, scaleTensor.Type, opName: "scale");
                 Operation scaled = graph.Mul(substracted, scaleOp);
                 Session session = new Session(graph);

                 Tensor imageTensor = image.CreateTensorFromBitmapByteArray(status);
                 Tensor[] imageResults = session.Run(new Output[] {input}, new Tensor[] {imageTensor},
                     new Output[] {scaled});
                 return imageResults[0];
             }
         }

         private static Tensor CreateTensorFromBitmapByteArray(this Bitmap image, Status status = null)
        {
            return Tensor.FromString(image.ToByteArray(), status);
        }

    }
}
﻿using System.Drawing;

namespace DrawingObjectsLibrary
{
    public sealed class RectangleObject : DrawingObject
    {
        public Rectangle Rectangle { get; private set; }

        public RectangleObject()
        {
            Rectangle = new Rectangle();
        }
        
        //kolor
        //rozmiar->szerokosc, wysokosc
        //lokalizacja - jedna, do rectangle

        public override Color Color { get; set; }
        public override Point Point { get; set; }
        public override Size Size { get; set; }

        public override void Draw(Bitmap bitmap)
        {
            throw new System.NotImplementedException();
        }
    }
}
using Emgu.CV.Structure;

namespace HandRecognitionLibrary.Structures
{
    public static class ColorsInScalar
    {
        public static MCvScalar BlueColor { get; } = new MCvScalar(255, 0, 0);
        public static MCvScalar GreenColor { get; } = new MCvScalar(0, 255, 0);
        public static MCvScalar RedColor { get; } = new MCvScalar(0, 0, 255);
        public static MCvScalar BlackColor { get; } = new MCvScalar(0, 0, 0);
        public static MCvScalar WhiteColor { get; } = new MCvScalar(255, 255, 255);
        public static MCvScalar YellowColor { get; } = new MCvScalar(0, 255, 255);
        public static MCvScalar PurpleColor { get; } = new MCvScalar(255, 0, 255);
    }
}
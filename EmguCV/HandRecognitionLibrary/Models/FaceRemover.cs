﻿using System;
using System.Drawing;
using Emgu.CV;
using Emgu.CV.CvEnum;
using HandRecognitionLibrary.Structures;

namespace HandRecognitionLibrary.Models
{
    public class FaceRemover
    {
        private CascadeClassifier _faceCascadeClassifier;
        private Rectangle _faceRectangle;
        private readonly string _faceClassifierFileName = ".\\Resources\\haarcascade_frontalface_alt.xml";
        public Rectangle[] FaceRectangles { get; private set; }


        public FaceRemover()
        {
            try
            {
                this._faceCascadeClassifier = new CascadeClassifier(this._faceClassifierFileName);
            }
            catch (Exception e)
            {
                throw new Exception("Exception: Nie udało się załadować klasyfikatorów FaceDetectora!", e);
            }
        }

        public void RemoveFaces(Mat input, Mat output)
        {
            if (FaceRectangles == null)
                PrepareFaceRectangles(input);

            for (int i = 0; i < FaceRectangles.Length; i++)
            {
                CvInvoke.Rectangle(
                    output,
                    FaceRectangles[i],
                    ColorsInScalar.BlackColor,
                    -1
                );
            }

            FaceRectangles = null;
        }

        public void PrepareFaceRectangles(Mat input)
        {
            Mat frameGray = new Mat();

            CvInvoke.CvtColor(input, frameGray, ColorConversion.Bgr2Gray);
            CvInvoke.EqualizeHist(frameGray, frameGray);

            FaceRectangles = _faceCascadeClassifier.DetectMultiScale(frameGray, 1.1D, 2);
        }

        public Rectangle GetFaceRectangle(Mat input)
        {
            Mat inputGray = new Mat();

            CvInvoke.CvtColor(input, inputGray, ColorConversion.Bgr2Gray);
            CvInvoke.EqualizeHist(inputGray, inputGray);

            var faceRectangles =
                _faceCascadeClassifier.DetectMultiScale(inputGray, 1.1, 2,
                    new Size(120, 120));

            if (faceRectangles.Length > 0)
                return faceRectangles[0];

            return new Rectangle(0, 0, 1, 1);
        }
    }
}
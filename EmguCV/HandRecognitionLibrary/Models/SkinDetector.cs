﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using HandRecognitionLibrary.Structures;

namespace HandRecognitionLibrary.Models
{
    public class SkinDetector
    {
        private int _hLowThreshold;
        private int _hHighThreshold;
        private int _sLowThreshold;
        private int _sHighThreshold;
        private int _vLowThreshold;
        private int _vHighThreshold;
        private bool _calibrated = false;
        private int _offsetHueLowThreshold = 27;
        private int _offsetHueHighThreshold = 100;
        private int _offsetSaturationLowThreshold = 50;
        private int _offsetSaturationHighThreshold = 80;

        private Rectangle _skinColorSamplerRectangle1, _skinColorSamplerRectangle2;

        public void DrawSkinColorSampler(ref Mat image)
        {
            int frameWidth = image.Width, frameHeight = image.Height;
            int rectangleSize = 20;

            _skinColorSamplerRectangle1 = new Rectangle(frameWidth / 5, frameHeight / 2, rectangleSize, rectangleSize);
            _skinColorSamplerRectangle2 = new Rectangle(frameWidth / 5, frameHeight / 3, rectangleSize, rectangleSize);

            CvInvoke.Rectangle(
                image,
                _skinColorSamplerRectangle1,
                ColorsInScalar.PurpleColor
            );

            CvInvoke.Rectangle(
                image,
                _skinColorSamplerRectangle2,
                ColorsInScalar.PurpleColor
            );
        }

        public void CalibrateFromFace(Mat input, Rectangle[] facesROI)
        {
            if (facesROI.Length == 0)
                return;

            Mat hsvInput = new Mat();
            CvInvoke.CvtColor(input, hsvInput, ColorConversion.Bgr2Hsv);

            Mat[] samples = new Mat[facesROI.Length];
            for (int i = 0; i < facesROI.Length; i++)
            {
                samples[i] = new Mat(hsvInput, facesROI[i]);
            }

            //CalculateThresholds(samples);
            CalculateThresholdsFromModes(samples);

            this._calibrated = true;
        }

        private MCvScalar CalculateMode(Mat[] faceRoi)
        {
            #region Prepare

            Matrix<Byte> valuesInRoi = new Matrix<byte>(faceRoi[0].Rows, faceRoi[0].Cols, faceRoi[0].NumberOfChannels);
            faceRoi[0].CopyTo(valuesInRoi);

            var splitedMatrix = valuesInRoi.Split();
            int size = faceRoi[0].Cols;
            var hueValues = splitedMatrix[(int) Channel.Hue].Data;
            var saturationValues = splitedMatrix[(int) Channel.Saturation].Data;

            var listHueValues = FlattenMatrix(hueValues, size);
            var listSaturationValues = FlattenMatrix(saturationValues, size);

            #endregion

            #region CalculateMode

            MCvScalar modeScalar = new MCvScalar();

            modeScalar.V0 = listHueValues.GroupBy(x => x).OrderByDescending(x => x.Count()).First().Key;
            modeScalar.V1 = listSaturationValues.GroupBy(x => x).OrderByDescending(x => x.Count()).First().Key;

            #endregion

            return modeScalar;
        }

        private IList<Byte> FlattenMatrix(byte[,] matrix, int size)
        {
            IList<Byte> flattenMatrix = new List<byte>();

            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    flattenMatrix.Add(matrix[i, j]);
                }
            }

            return flattenMatrix;
        }

        private void CalculateThresholds(Mat[] faceRoi)
        {
            MCvScalar[] hsvMeansSamples = new MCvScalar[faceRoi.Length];
            for (int i = 0; i < faceRoi.Length; i++)
            {
                hsvMeansSamples[i] = CvInvoke.Mean(faceRoi[i]);
            }

            _hLowThreshold = Convert.ToInt32(hsvMeansSamples.Min(x => x.V0) - _offsetHueLowThreshold);
            _hHighThreshold = Convert.ToInt32(hsvMeansSamples.Max(x => x.V0) + _offsetHueHighThreshold);

            _sLowThreshold = Convert.ToInt32(hsvMeansSamples.Min(x => x.V1) - _offsetSaturationLowThreshold);
            _sHighThreshold = Convert.ToInt32(hsvMeansSamples.Max(x => x.V1) + _offsetSaturationHighThreshold);

            _vLowThreshold = 40;
            _vHighThreshold = 255; 
        }

        private void CalculateThresholdsFromModes(Mat[] faceRoi)
        {
            MCvScalar hsvModeSample = CalculateMode(faceRoi);

            if (hsvModeSample.V0 != 0 && hsvModeSample.V1 != 0)
            {
                _hLowThreshold = Convert.ToInt32(hsvModeSample.V0 - 35);
                _hHighThreshold = Convert.ToInt32(hsvModeSample.V0 + 35);

                _sLowThreshold = Convert.ToInt32(hsvModeSample.V1 - 50);
                _sHighThreshold = Convert.ToInt32(hsvModeSample.V1 + 50);
            }

            _vLowThreshold = 10;
            _vHighThreshold = 255;
        }

        public void CalculateThresholds(Mat sample1, Mat sample2)
        {
            MCvScalar hsvMeansSample1 = CvInvoke.Mean(sample1);
            MCvScalar hsvMeansSample2 = CvInvoke.Mean(sample2);

            _hLowThreshold =
                Convert.ToInt32(Math.Min(hsvMeansSample1.V0, hsvMeansSample2.V0) - _offsetHueHighThreshold);
            _hHighThreshold =
                Convert.ToInt32(Math.Max(hsvMeansSample1.V0, hsvMeansSample2.V0) + _offsetHueHighThreshold);

            _sLowThreshold =
                Convert.ToInt32(Math.Min(hsvMeansSample1.V1, hsvMeansSample2.V1) - _offsetSaturationLowThreshold);
            _sHighThreshold =
                Convert.ToInt32(Math.Max(hsvMeansSample1.V1, hsvMeansSample2.V1) + _offsetSaturationHighThreshold);

            _vLowThreshold = 0;
            _vHighThreshold = 255;
        }

        public Mat GetSkinMask(Mat input)
        {
            Mat skinMask = new Mat();

            if (!_calibrated)
            {
                skinMask = Mat.Zeros(input.Rows, input.Cols, DepthType.Cv8U, input.NumberOfChannels);
                return skinMask;
            }

            Mat hsvInput = new Mat();
            CvInvoke.CvtColor(input, hsvInput, ColorConversion.Bgr2Hsv);

            CvInvoke.InRange(
                hsvInput,
                new ScalarArray(new MCvScalar(_hLowThreshold, _sLowThreshold, _vLowThreshold)),
                new ScalarArray(new MCvScalar(_hHighThreshold, _sHighThreshold, _vHighThreshold)),
                skinMask
            );

            this.Opening(skinMask, ElementShape.Ellipse, new Point(1, 1));
            CvInvoke.Dilate(skinMask, skinMask, new Mat(), new Point(-1, -1), 2, BorderType.Default, new MCvScalar());

            return skinMask;
        }

        private void Opening(Mat binaryImage, ElementShape kernelShape, Point kernelSize)
        {
            Mat structuringElement = CvInvoke.GetStructuringElement(kernelShape,
                new Size(2 * kernelSize.X + 1, 2 * kernelSize.Y + 1), kernelSize);
            CvInvoke.MorphologyEx(binaryImage, binaryImage, MorphOp.Open, structuringElement, new Point(-1, -1), 1,
                BorderType.Constant, CvInvoke.MorphologyDefaultBorderValue);
        }

        private void Closing(Mat binaryImage, ElementShape kernelShape, Point kernelSize)
        {
            Mat structuringElement = CvInvoke.GetStructuringElement(kernelShape,
                new Size(2 * kernelSize.X + 1, 2 * kernelSize.Y + 1), kernelSize);
            CvInvoke.MorphologyEx(binaryImage, binaryImage, MorphOp.Close, structuringElement, new Point(-1, -1), 4,
                BorderType.Constant, CvInvoke.MorphologyDefaultBorderValue);
        }

        private enum Channel
        {
            Hue = 0,
            Saturation = 1,
        }
    }
}

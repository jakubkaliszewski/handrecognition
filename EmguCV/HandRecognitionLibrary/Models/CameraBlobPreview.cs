﻿namespace HandRecognitionLibrary.Models
{
    public class CameraBlobPreview : CameraPreview
    {
        public CameraBlobPreview(string windowName)
            : base(windowName)
        {
        }

        public void OnCreatedBlob(object sender, HandBlobFromCameraEventArgs e)
        {
            Viewer.Image = e.Blob;
        }
    }
}
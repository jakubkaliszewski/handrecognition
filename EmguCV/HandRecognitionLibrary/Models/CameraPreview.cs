﻿using System;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Emgu.CV;
using Emgu.CV.UI;

namespace HandRecognitionLibrary.Models
{
    public class CameraPreview
    {
        protected ImageViewer Viewer;

        public CameraPreview(string windowName)
        {
            Viewer = new ImageViewer(); //create an image viewer
            SetWindowProperties(windowName);
        }

        public void StartPreview()
        {
            try
            {
                Viewer.ShowDialog(); //show the image viewer
            }
            catch (InvalidOperationException e)
            {
                throw new Exception($"Exception: {e}", e);
            }
        }

        public void ClosePreview()
        {
            try
            {
                Viewer.Close();
            }
            catch (Exception e)
            {
                throw new Exception($"Exception: {e}", e);
            }
        }

        public void SaveImage()
        {
            DateTime date = DateTime.Now;
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("{0}-{1}_{2}-{3}-{4}.png", date.Day, date.Month, date.Hour, date.Minute, date.Second);
            string path = sb.ToString();

            try
            {
                Viewer.Image.Bitmap.Save(path);
            }
            catch (Exception e)
            {
                throw new Exception($"Saving image problem: {e}");
            }
        }

        protected void SetWindowProperties(string windowName)
        {
            Viewer.Width = Helpers.ImageWidth;
            Viewer.Height = Helpers.ImageHeight + 50;
            Viewer.Text = windowName;
            Viewer.StartPosition = FormStartPosition.Manual;
            Viewer.Location = new Point(0, 0);
            Viewer.MaximizeBox = false;
            Viewer.MinimizeBox = false;
            Viewer.FormBorderStyle = FormBorderStyle.FixedSingle;
        }

        public void OnCapturedFrameFromCamera(object sender, CameraEventArgs e)
        {
            Viewer.Image = e.Image;
        }
    }
}
﻿namespace HandyPaintNET
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
            cameraHelperThread.Abort();
            cameraHelper.Dispose();

        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.setBackgroundButton = new System.Windows.Forms.Button();
            this.saveActualContoursFrameButton = new System.Windows.Forms.Button();
            this.predictionLabel = new System.Windows.Forms.Label();
            this.GestureLabel = new System.Windows.Forms.Label();
            this.GestureValue = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // setBackgroundButton
            // 
            this.setBackgroundButton.Location = new System.Drawing.Point(612, 386);
            this.setBackgroundButton.Name = "setBackgroundButton";
            this.setBackgroundButton.Size = new System.Drawing.Size(176, 52);
            this.setBackgroundButton.TabIndex = 1;
            this.setBackgroundButton.Text = "Ustaw tło";
            this.setBackgroundButton.UseVisualStyleBackColor = true;
            this.setBackgroundButton.Click += new System.EventHandler(this.setBackgroundButton_Click);
            // 
            // saveActualContoursFrameButton
            // 
            this.saveActualContoursFrameButton.Location = new System.Drawing.Point(12, 386);
            this.saveActualContoursFrameButton.Name = "saveActualContoursFrameButton";
            this.saveActualContoursFrameButton.Size = new System.Drawing.Size(176, 52);
            this.saveActualContoursFrameButton.TabIndex = 2;
            this.saveActualContoursFrameButton.Text = "Zapisz obraz z podglądu";
            this.saveActualContoursFrameButton.UseVisualStyleBackColor = true;
            this.saveActualContoursFrameButton.Click += new System.EventHandler(this.saveActualContoursFrameButton_Click);
            // 
            // predictionLabel
            // 
            this.predictionLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.predictionLabel.AutoSize = true;
            this.predictionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.predictionLabel.ForeColor = System.Drawing.Color.Green;
            this.predictionLabel.Location = new System.Drawing.Point(291, 160);
            this.predictionLabel.Name = "predictionLabel";
            this.predictionLabel.Size = new System.Drawing.Size(220, 63);
            this.predictionLabel.TabIndex = 3;
            this.predictionLabel.Text = "Etykieta";
            this.predictionLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // GestureLabel
            // 
            this.GestureLabel.AutoSize = true;
            this.GestureLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.GestureLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 22F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.GestureLabel.Location = new System.Drawing.Point(296, 301);
            this.GestureLabel.Name = "GestureLabel";
            this.GestureLabel.Size = new System.Drawing.Size(102, 36);
            this.GestureLabel.TabIndex = 4;
            this.GestureLabel.Text = "Gest?:";
            this.GestureLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // GestureValue
            // 
            this.GestureValue.AutoSize = true;
            this.GestureValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 22F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.GestureValue.ForeColor = System.Drawing.Color.Green;
            this.GestureValue.Location = new System.Drawing.Point(400, 300);
            this.GestureValue.Name = "GestureValue";
            this.GestureValue.Size = new System.Drawing.Size(124, 36);
            this.GestureValue.TabIndex = 5;
            this.GestureValue.Text = "Tak/Nie";
            this.GestureValue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.GestureValue);
            this.Controls.Add(this.GestureLabel);
            this.Controls.Add(this.predictionLabel);
            this.Controls.Add(this.saveActualContoursFrameButton);
            this.Controls.Add(this.setBackgroundButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "HandyPaintNET";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button setBackgroundButton;
        private System.Windows.Forms.Button saveActualContoursFrameButton;
        public System.Windows.Forms.Label predictionLabel;
        private System.Windows.Forms.Label GestureLabel;
        private System.Windows.Forms.Label GestureValue;
    }
}

